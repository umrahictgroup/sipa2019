<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
$module_path = "\App\Http\Modules";

Route::get('/', $module_path.'\Dashboard\Controllers\HomeController@index');
Route::get('/auth', $module_path.'\Sistem\Controllers\AuthController@index');
Route::get('/auth/logout', $module_path.'\Sistem\Controllers\AuthController@logout');
Route::post('/auth/do_login', $module_path.'\Sistem\Controllers\AuthController@do_login');
