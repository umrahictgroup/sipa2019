<?php
namespace App\Helpers;

class TableH{
	public const TB_MENU = "menu";
	public const TB_CAPABILITIES = "capabilities";
	public const TB_POST = "post";
	public const TB_POST_META = "post_meta";
	public const TB_KALENDER = "kalender_akademik";
	public const TB_KALENDER_DETAIL = "kalender_akademik_detail";
}