<?php
namespace App\Helpers;
use Request;
use View;

class ViewH{
  public static function load_view($page_name='', $data = array(), $with_user_info = false){
    $page = Request::input("p");
		if(empty($page))
			$page = Request::post("p");
    
    if($page == 1)
      return view($page_name, $data);
    else{
      //return View::make('layout/header', [])
      //->nest('menu','layout/menu', [])->nest('content',$page_name, $data);
      $view = View::make('layout/container')
      ->nest('header', 'layout/header')
      ->nest('menu', 'layout/menu', $page_name=='dashboard/index'?$data:['menus'=>[]])
      ->nest('content', $page_name, $data)
      ->nest('footer', 'layout/footer');
      //$view->nest($page_name, $page_name);
      return $view;
    }
  }
}
