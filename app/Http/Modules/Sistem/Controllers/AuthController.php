<?php
namespace App\Http\Modules\Sistem\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Modules\Sistem\Models\UsersModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use ViewHelper;

class AuthController extends Controller{
  use AuthenticatesUsers;
  protected $username = 'username';

  public function __construct(){
    $this->model = new UsersModel();
  }

  public function index(){
    return view('auth/index');
  }

  public function do_login(Request $request){
    $validator = Validator::make(
      $request->all(),
      array(
        'username'=>'required',
        'password'=>'required'
      ),
      array(
        'required'=>':attribute tidak boleh kosong'
      )
    )->validate();

    if($this->model->do_login_user($request->username, $request->password)){
      $user_data = $this->model->get_userdata($request->username);
      if($user_data){
        $user_session 	= array('username'	=> $request->username,
  						'logged_in'     	=> TRUE,
  						'user_code'     	=> $user_data->user_code,
  						'user_id'			=> $user_data->ID,
  						'user_displayname'	=> $user_data->display_name,
  						'user_level'    	=> $user_data->user_status,
  						'original_level'	=> $user_data->user_status
  			);
        $request->session()->push('login',$user_session);
        return redirect('/');
      }
      else
        return redirect('/auth');
    }
    
    else{
      $validator = Validator::make(
        array('not_match'=>''),
        array(
          'not_match'=>'required',
        ),
        array(
          'not_match.required'=>'Nama pengguna dan kata sandi tidak cocok !'
        )
      );
      return redirect('/auth')->withErrors($validator, 'login');
    }
  }

  public function logout(Request $request){
    $request->session()->flush();
    return redirect('/auth');
  }
}
