<?php
namespace App\Http\Modules\Sistem\Models;
use Illuminate\Database\Eloquent\Model;
use TableHelper;

class MenuModel extends Model{
  protected $table = TableHelper::TB_MENU;

  public function get_all_menu(){
    $data = MenuModel::orderBy('id','asc')
                      ->get();
    return $data;
  }

  public function get_all_parentmenu(){
    $data = $this->db->select()
                         ->from($this->tb_menu)
                         ->where('sub_id','=',0)
                         ->orderBy('id','ASC')
                         ->getAll();
    return $data;
  }


  public function get_menu($user_level){
      $user_model = new UsersModel();
      $capabilities = $user_model->get_user_capabilities($user_level);
      $q_cap = '';
      $arr_cap = [];
      foreach($capabilities as $cap){
        $arr_cap[] = $cap;
        //$q_cap .= "'".$cap."',";
      }
      $arr_cap[] = 'read';
      $q_cap .= "'read'";

      $parent_level = MenuModel::whereIn('capabilities', $arr_cap)
                      ->where('sub_id', 0)
                      ->where('enabled', 'yes')
                      ->orderBy('display_name', 'asc')
                      ->get();
      return $parent_level;
    }

  public function get_submenu($parent_id = '', $user_level ){
    $user_model = new UsersModel();
    $capabilities = $user_model->get_user_capabilities($user_level);
    
        /*$data = $this->db->results("SELECT * FROM $this->tb_menu 
                                    WHERE capabilities IN ($q_cap) 
                                    AND sub_id = '$parent_id' 
                                    AND enabled = 'yes'
                                    ORDER BY orderline ASC, display_name ASC");*/
      $q_cap = '';
      $arr_cap = [];
      foreach($capabilities as $cap){
        $arr_cap[] = $cap;
        //$q_cap .= "'".$cap."',";
      }
      $arr_cap[] = 'read';
      $q_cap .= "'read'";

      $data = MenuModel::whereIn('capabilities', $arr_cap)
                      ->where('sub_id', $parent_id)
                      ->where('enabled', 'yes')
                      ->orderByRaw('orderline asc, display_name asc')
                      ->get();
    return $data;
  }

  public function build_menu($user_level){
    $parent_level = $this->get_menu($user_level);
    $menus = array();
    foreach($parent_level as $parent){
      $menus[$parent->ID] = array(
        'id'            => $parent->ID,
        'name'          => $parent->name,
        'display_name'  => $parent->display_name,
        'url'           => $parent->url,
        'icons'         => $parent->icons,
        'capabilities'  => $parent->capabilities
      );

      //submenu
      $submenu = $this->get_submenu($parent->ID, $user_level);
      foreach($submenu as $sub){
        $menus[$parent->ID]['submenu'][] = array(
          'id'            => $sub->ID,
          'name'          => $sub->name,
          'display_name'  => $sub->display_name,
          'url'           => $sub->url,
          'icons'         => $sub->icons,
          'capabilities'  => $sub->capabilities
                                                        );
      }
    }
    return $menus;
  }

  public function save_new_menu( $post = '' )
    {
        $this->db->insert($this->tb_menu, $post);
        return TRUE;
    }

    public function delete_menu( $menu_name = '')
    {
        $this->db->delete( $this->tb_menu, array('name' => $menu_name));
        return TRUE;
    }
}
