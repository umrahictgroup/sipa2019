<?php
namespace App\Http\Modules\Sistem\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UsersModel extends Model{
    protected $table = 'users';
    protected $tb_auth = 'auth.v0_tw_user';
    protected $tb_ucapabilities = 'usercapabilities';

    public function get_userdata($username=0){
      return UsersModel::where('user_login', $username)->first();
    }

    public function do_login_user($username='', $password){
      if(!empty($username)){
        $passwd = DB::table($this->tb_auth)
                  ->select('pass')
                  ->where('username', $username)
                  ->first();
        //print_r($passwd);
        //die();
        if($passwd){
          if($passwd->pass == md5($password))
            return true;
        }
        else{
          $passwd = DB::table($this->table)->select('user_pass')
                    ->where('user_login', $username)
                    ->first();
          if($passwd){
            if( $passwd->user_pass == md5($password))
              return true;
            return false;
          }
          return false;
        }
      }
      return false;
  }

  public function get_user_capabilities( $user_level ){
    $data = DB::table($this->tb_ucapabilities)
            ->where('level_number', $user_level)
            ->first();
    $data = unserialize($data->capabilities);
    return $data;
  }
}
