<?php 
namespace App\Http\Modules\Perkuliahan\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use TableHelper;

class KalenderModel extends Model{
	protected $table = TableHelper::TB_POST;
	
	public function simpan($data){
		return DB::table(TableHelper::TB_KALENDER_DETAIL)->insert($data);
  	}


	public function save_new_post( $data ){
		$data['event_date']	= date("Y-m-d H:i:s");
		$last_id = DB::table(TableHelper::TB_KALENDER_DETAIL)->insertGetId($data);

		if(!empty($last_id) || !is_null($last_id ) )
		{
			$results = array('return' => TRUE, 'msg' => "Sukses menerbitkan berita baru dengan judul $data[title].
				ID berita: $last_id",
				'returned_id' => $last_id );
			return json_encode($results);
		}else{
			$results = array('return' => FALSE, 'msg' => "Gagal menerbitkan berita baru dengan judul $data[title]");
			return json_encode($results);
		}
	}

  	function getDateEvent($year, $month){
	    $year  = ($month < 10 && strlen($month) == 1) ? "$year-0$month" : "$year-$month";
	    $query = 
	    $query = $this->db->select('event_date, total_events')->from('events')->like('event_date', $year, 'after')->get();
	    if($query->num_rows() > 0){
	      $data = array();
	      foreach($query->result_array() as $row){
	        $ddata = explode('-',$row['event_date']);
	        $data[(int) end($ddata)] = $row['total_events'];
	      }
	      return $data;
	    }else{
	      return false;
	    }
	}

  	function getEvent(){
  		$data = DB::table(TableHelper::TB_KALENDER_DETAIL)
  				->whereRaw('DATE(tanggal_mulai) = DATE(NOW())')
  				->get();

  		return $data;
  	}

	public function save_edit_post( $data )
	{
		$data['time_update']	= date("Y-m-d H:i:s");
		$data['ip_update']		= $_SERVER['REMOTE_ADDR'];

		$arr_wh = array('id' => $data['id']);
		$id_post = $data['id'];
		unset($data['id']);

		$this->db->update($this->tb_news, $data, $arr_wh);

		$cek					= $this->db->row("SELECT content FROM $this->tb_news WHERE id = '$id_post' LIMIT 1");

		if( $cek->content == $data['content'] )
		{
			$results = array('return' => TRUE, 'msg' => "Sukses mengedit berita dengan judul $data[title].
				ID berita: $id_post",
				'returned_id' => $id_post );
			return json_encode($results);
		}else{
			$results = array('return' => FALSE, 'msg' => "Gagal mengedit berita  dengan judul $data[title]");
			return json_encode($results);
		}
	}

	public function get_news_data( $id )
	{
		$data = $this->db->select()
						 ->from($this->tb_news)
						 ->where('id','=',$id)
						 ->getOne();
		return $data;
	}

	public function get_all_news( $page = 1, $limit = 5 )
	{
		$offset = ($limit * $page) - $limit;

		$data = $this->db->results("SELECT * FROM $this->tb_news
									WHERE type = 'news'
									ORDER BY id DESC
									LIMIT $offset, $limit
									");

		return $data;
	}

	public function unpublished_post( $id = 1)
	{
		$id = (int) $id;

		$this->db->update($this->tb_news, array('status' => 'pending'), array('id' => $id ));
		$cek = $this->db->find_var("SELECT status FROM $this->tb_news WHERE id='$id' LIMIT 1");

		if($cek == 'pending')
		{
			$results = array('return' => TRUE,
				'msg' => "Sukses mengubah status berita dari published ke pending untuk ID: $id");
			return json_encode($results);
		}else{
			$results = array('return' => FALSE,
				'msg' => "Gagal mengubah status berita dari published ke pending untuk ID: $id");
			return json_encode($results);
		}
	}

	public function get_list_notification( $smstr = '', $nim = '')
	{
		$tabel = $smstr.'_notification';
		return $this->db->results("SELECT * FROM $tabel WHERE nim = '$nim' ORDER BY ID DESC");
	}
}