<?php
namespace App\Http\Modules\Dashboard\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Modules\Sistem\Models\UsersModel;
use App\Http\Modules\Sistem\Models\MenuModel;
use App\Http\Modules\Perkuliahan\Models\KalenderModel;
use Illuminate\Http\Request;
use ViewHelper;
use SecurityHelper;

class HomeController extends Controller{

  public function __construct(){
    SecurityHelper::check_login();
    $this->siakad_config	=  config('akademik_umum');
    $this->menu_model = new MenuModel();
    $this->kakad_model = new KalenderModel();
  }

  public function index(Request $request){
    $userlevel = $request->session()->get('login.0.user_level');
    $_semesterakademik		= $request->session()->get('id_tahunakademik_default');
    if(empty($_semesterakademik))
      $data['smstr']		= $this->siakad_config['id_tahunakademik_default'];

    $data['userlevel'] = $userlevel;
    $data['menus'] = $this->menu_model->build_menu($userlevel);
    $data['kalender'] = $this->kakad_model->getEvent();
    return ViewHelper::load_view('dashboard/index', $data);
  }
}
