<?php
/**
	Konfigurasi Umum SIAKAD
**/

return array(
	'id_tahunakademik_default'	=> '5',
	'str_tahunakademik_default' => 'R119',
	'str_tahunakademik_yudisium'=> 'R119',
	'is_used_cache'				=> 0,
	'tahun_ekuivalensi'			=>2019
);
