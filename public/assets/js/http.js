var BaseUrl = "http://localhost/kopin/index.php/"; 
async function HttpPost(to, formData, type='json'){
	return new Promise((resolve, reject)=>{
		fetch(BaseUrl+to, {method:'POST', body:formData})
		.then(function(response){
			if(type == 'json')
				return response.json();
			else if(type='text')
				return response.text();
		})
		.then((res)=>{
			resolve(res)
		})
		.catch((error)=>{
			reject(error)
		})
	});
}

function HttpGet(to, formData, type='') {
    return new Promise((resolve, reject) => {
        fetch(to)
        .then(function(response){
			if(type == 'json')
				return response.json();
			else if(type='text')
				return response.text();
		})
        .then((res) => {
                resolve(res)
        })
        .catch((error) => {
        	reject(error)
      	})
    })
}