function open_menu(name, title, url){
	$('body').scrollTop(0);
	window.history.pushState(name, title, url);
	$('#loading').modal('open');
	HttpGet(document.location.href+"?p=1", 'text')
	.then(result=>{
		$("#content :hidden").remove();
		$('#content').html(result);
		$('#content').children().unwrap();		
		$('.collapsible').collapsible();
		$('#loading').modal('close');
		$('body').css('overflow-y','auto');
	});
}

function load_table(id, url){
	$('#'+id).load(url, function(){
		 $(this).children().unwrap();
	});
}

function search(element, url, keywords){
	let form = new FormData();
	form.append('keywords', filterXSS(keywords));
	HttpPost(url, form, 'text')
	.then(result=>{
		$(element).html(result);
		$(element).children().unwrap();
	});
}

function search_filter(element, url, form){
	HttpPost(url, form, 'text')
	.then(result=>{
		$(element).html(result);
		$(element).children().unwrap();
	});
}

function refresh_select(){
	$('select').formSelect();
	$('select').formSelect('destroy');
	$('select').formSelect();
}

function scrollToAnchor(aid){
    var aTag = $("a[name='"+ aid +"']");
    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
}

function currencyFormat(num) {
  return (
    num
      .toFixed() // always two decimal digits
      .replace('.', ',') // replace decimal point character with ,
      .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  ) // use . as a separator
}


function scroll_to(element){
	//window.scrollTo(0, $(element).offset().top);
	$('html, body').animate({ scrollTop: $(element).offset().top }, 'slow');
}

function open_loading(){
	$('#loading').modal('open');
}

function close_loading(){
	$('#loading').modal('close');
}

function do_cetak(url, name, params){
	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("action", url);
	form.setAttribute("target", name);
	for (var i in params){
		if (params.hasOwnProperty(i)){
	    	var input = document.createElement('input');
	     	input.type = 'hidden';
	     	input.name = i;
	     	input.value = params[i];
	     	form.appendChild(input);
	   	}
	}
	document.body.appendChild(form);
	form.submit();
	document.body.removeChild(form);
}

function previewImage(input, img) {
    if (input.files && input.files[0]) {
    	var reader = new FileReader();            
        reader.onload = function (e) {
        $(img).attr('src', e.target.result);
    }            
    reader.readAsDataURL(input.files[0]);
	}
}