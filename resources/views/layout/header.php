<html>
	<head>
		<meta http-equiv="imagetoolbar" content="no" />
		<meta charset="utf-8" />
		<title>SIPA UMRAH 2019 </title>
		<link rel="icon" type="image/png" href="<?php echo asset('/assets/img/favicon.ico')?>"/>
		<meta name="description" content="Aplikasi Sistem Informasi Akademik UMRAH Tahun 2019 Versi 2" />
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"
  				integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  				crossorigin="anonymous">
  	</script>
		<link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
		<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('/assets/css/other.css')?>">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('/assets/css/breadcrumb.css')?>">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
	  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
   	<script src="https://rawgit.com/vuejs-tips/v-money/master/dist/v-money.js"></script>
	  <script src="https://rawgit.com/leizongmin/js-xss/master/dist/xss.js"></script>
		<script src="<?php echo asset('/assets/js/http.js')?>"></script>
	  <script src="<?php echo asset('/assets/js/general.js')?>"></script>

	    <!-- jquery ui !-->
	  <script src="<?php echo asset('/assets/js/ui/jquery-ui.min.js')?>"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo asset('/assets/css/jquery-ui.min.css')?>">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('/assets/css/jquery-ui.structure.min.css')?>">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('/assets/css/jquery-ui.theme.min.css')?>">

	    <!-- Material Step!-->
	  <link rel="stylesheet" href="https://unpkg.com/materialize-stepper@3.0.0-beta.1/dist/css/mstepper.min.css">
 		<script src="https://unpkg.com/materialize-stepper@3.0.0-beta.1/dist/js/mstepper.min.js"></script>

 		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/highcharts-3d.js"></script>
		
		<style>
			.sidenav li>a{
				color:#fafafa;
				font-size: 13px;
			}
			.sidenav li>a>i, .sidenav li>a>[class^="mdi-"], .sidenav li>a li>a>[class*="mdi-"], .sidenav li>a>i.material-icons{
				color:#fafafa;
				font-size: 15px;    
				margin-right: 10px;
			}
			h4{
				font-size: 1.2em;
			}
		</style>
	</head>
<body class="no-skin" style="padding-top: 0px;overflow: hidden;">
