<div id="loading" class="modal loading_modal">
      <div class="preloader-wrapper active" style="display: table;margin:0 auto">
      <div class="spinner-layer spinner-red-only">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>
    </div>
</div>
</body>
<style type="text/css">
  .loading_modal{
    position: absolute;
    top:50vh !important;
    overflow: hidden;
    width: 90px;
    padding: 20px;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
    $('.modal').modal({dismissible:false});
  });
</script>
</html>
