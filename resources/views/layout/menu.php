<script type="text/javascript">
	window.onpopstate = function (event) {
	  if (event.state) {
	    $('#content').load(document.location.href+"?p=1",function(){
			 $(this).children().unwrap();
		});
	  }
	}

	function logOut(){
		window.location.href = "index.php/auth/logout";
	}

	$(document).ready(function(){
		$('.sidenav').sidenav();
		$('.collapsible').collapsible();
		$('.tooltipped').tooltip();
		$('.dropdown-trigger').dropdown();
		$('.dropdown-button').dropdown({
			inDuration: 300,
			outDuration: 225,
			constrain_width: true, // Does not change width of dropdown to that of the activator
			hover: true, // Activate on hover
			gutter: 0, // Spacing from edge
			belowOrigin: true, // Displays dropdown below the button
			alignment: 'right' // Displays dropdown with edge aligned to the left of button
			}
		);
	});
</script>
<?php
	$img_src = asset('assets/img/user.png');
	//if(!empty($decode->userinfo->avatar))
		//$img_src = $decode->userinfo->avatar;
	//print_r($menus);
?>
<div class="navbar-fixed">
	<nav>
		<div class="nav-wrapper green darken-3" style="padding-left: 20px">
		<span class="sidenav-trigger" data-target="slide-out" >
			<a href="#" class="brand-logo" >
				<i class="material-icons">menu</i>
			</a>
		</span>
		<ul class="right hide-on-med-and-down" style="padding-top: 10px;">
			 <li style="position: absolute;top: 0">
			 	<a href="javascript:void(0)" id="meta" data-target="level" class="dropdown-trigger" >
			 		<span class="white-text email">
			 			<?php //echo $decode->userinfo->level?>
			 		</span>
			 	</a>
			 </li>
		      <li>
		      	<div class="user-view" style="padding: 0px;width: auto;" >
		      		<div style="width: 180px">
		      		<a href="javascript:void(0)" class="dropdown-trigger" data-target='user_profil'>
			              <img src="<?php echo $img_src?>"
			              alt="" class="avatar" style="width: 50px;height: 50px;top:7; position: absolute;right: 4;"/> <!-- notice the "circle" class -->
			        </a>
			    </div>
		      	</div>
		      </li>
		  </ul>
		</div>
	</nav>
</div>
<ul id="slide-out" class="sidenav sidenav-fixed" style="background-color: #222c3c">
	<li>
		<div class="user-view" style="padding: 0px; padding-top: 10px">
      <div class="background">
        <img style="width: 100%;" src="https://dashboard.zawiastudio.com/demo/img/project-card4.jpg">
      </div>
      <div style="padding-left: 10px;padding-bottom: 60px">
	      <a href="javascript:void(0)">
	      	<img class="avatar" src="<?php echo $img_src?>" style="width: 70px;height: 70px;top:5;position: absolute;left: 5;padding-bottom: "></a>
      </div>
      <div style="background-color: #00000091; width: 100%;padding-left: 10px;">
	      <span class="white-text name" style="font-weight: bold;"><?php //echo $decode->userinfo->nama_lengkap?></span>
	      <a href="javascript:void(0)" data-target="level2" class="dropdown-trigger"><span class="white-text email"><?php //echo $decode->userinfo->level?></span></a>
	  </div>
    </div></li>

	<li>
		<a class="sidenav-close" href="javascript:void(0)" id = "dashboard"
		onclick="open_menu('home', '', '<?php echo url('/')?>');">
		<i class="fa fa-dashboard" style="padding-left: 4px;"></i>Dashboard</a>
	</li>
	<?php
    	foreach($menus as $menu):?>
    		<ul class="collapsible collapsible-accordion" >
				<li>
            		<a class="collapsible-header">
            		<i class="fa fa-<?php echo $menu['icons']?>" style="margin-left:15px">
            		</i> <?php echo $menu['display_name']?>
            		</a>
            		<?php if(array_key_exists('submenu', $menu)):?>
            			<div class="collapsible-body" style="background-color: rgb(41, 52, 69);">
	             		<ul>
            			<?php foreach($menu['submenu'] as $submenu):?>
	             			<li>
	             				<a onclick = "open_menu('<?php echo $submenu['name']?>', '', '<?php echo url($submenu['url'])?>')" href="javascript:void(0)" class="sidenav-close">
							<i class="material-icons" >keyboard_arrow_right</i><?php echo $submenu['display_name']?></a></li>
             			
             		<?php 
             			endforeach; ?>
             			</ul>
             		</div>
             		<?php
             		endif;
             		?>
            	</li>
            </ul>
			<?php
					
        endforeach;
        ?>
	<?php
	//generate_menu($decode->userinfo->id_level);
	?>
	<li><a href="javascript:void(0)"  onclick="logOut()"><i class="material-icons"  style="padding-left: 4px;">power_settings_new</i>Logout</a></li>
</ul>


<?php
	//print_r(json_decode($decode->userinfo->usermeta));

	/*$meta = json_decode($meta_user);
	$level_meta = "";
	$no = 0;
	if(!empty($meta)){
		foreach ($meta as $row) {
			if(!empty($row->value)){
				$enc = sardi_enc($row->value);
				$level_meta.="<li><a class='truncate' onclick='change_level(\"".$enc."\")' href='javascript:void(0)'>".$row->id."</a></li>";
				$no++;
			}
		}
		if($no>0){
			$level_meta.="<li><a class='truncate' onclick='back_level()' href='javascript:void(0)'>Kembali</a></li>";
			$user_old = sardi_enc($decode->userinfo->username);
			?>
		<script>
			function change_level(id){
				let form_change = new FormData();
				form_change.append('id', id);
				HttpPost('users/change_level', form_change)
				.then(result=>{
					if(result.error_code == 0)
						location.reload();
				})
			}

			function back_level(){
				let form_change = new FormData();
				form_change.append('is_back', 1);
				HttpPost('users/change_level', form_change)
				.then(result=>{
					if(result.error_code == 0)
						location.reload();
				})
			}
		</script>
		<?php
		}
	}*/
?>
<ul id="level" class='dropdown-content' style="overflow-y: visible;">
    <?php //echo $level_meta?>
</ul>

<ul id="level2" class='dropdown-content' style="overflow-y: visible;">
    <?php //echo $level_meta?>
  </ul>

<ul id="user_profil" class="dropdown-content" style="overflow-y: visible;">
	<li><a href="javascript:void(0)" onclick="open_menu('edit_profil','', '<?php echo url('/users/edit_profil')?>')">Edit Profil</a></li>
	</li>
	<li><a href="javascript:void(0)" onclick="logOut()">Keluar</a></li>
</ul>

<script>
	$(document).ready(function(){
		$('#meta').click(function(){
			$('#level').css('top',10);
		});
	});
</script>
