<!DOCTYPE html>
<html lang="en">
<head>
	<title>SIPA 2019 - Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?php echo asset('/assets/img/favicon.ico')?>"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo asset('/assets/vendor/bootstrap/css/bootstrap.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo asset('/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo asset('/assets/fonts/iconic/css/material-design-iconic-font.min.css')?>">
<!--===============================================================================================-->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo asset('/assets/vendor/css-hamburgers/hamburgers.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo asset('/assets/vendor/animsition/css/animsition.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo asset('/assets/vendor/select2/select2.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo asset('/assets/vendor/daterangepicker/daterangepicker.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo asset('/assets/css/util.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo asset('/assets/css/main.css')?>">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<!--===============================================================================================-->
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="POST" action="<?php echo url('/auth/do_login')?>">
          <?php echo csrf_field()?>
					<span class="login100-form-title p-b-26">
						SIPA 2019
					</span>
					<span class="login100-form-title p-b-48">
						<img src="<?php echo asset('/assets/img/logo.png')?>" width="40%"/>
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Masukkan nama pengguna">
						<input class="input100" type="text" name="username">
						<span class="focus-input100" data-placeholder="Nama Pengguna"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Masukkan kata sandi">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="password">
						<span class="focus-input100" data-placeholder="Kata Sandi"></span>
					</div>

					<?php if($errors->login->has('not_match')){
						echo '<div class="alert alert-danger">'.$errors->login->first('not_match').'</div>';
					}?>
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Sign In
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="<?php echo asset('/assets/vendor/jquery/jquery-3.2.1.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo asset('/assets/vendor/animsition/js/animsition.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo asset('/assets/vendor/bootstrap/js/popper.js')?>"></script>
	<script src="<?php echo asset('/assets/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo asset('/assets/vendor/select2/select2.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo asset('/assets/vendor/daterangepicker/moment.min.js')?>"></script>
	<script src="<?php echo asset('/assets/vendor/daterangepicker/daterangepicker.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo asset('/assets/vendor/countdowntime/countdowntime.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo asset('/assets/js/main.js')?>"></script>

</body>
</html>
