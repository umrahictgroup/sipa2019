<div id="content">
  <main id="app">
    <h4>Selamat Datang <small>di Sistem Informasi Pengelolaan Akademik</small></h4>
    <div class="row">
    	<div class="col s12">
    		<div class="card">
    			<div class="card-content">
	    			<span class="card-title">KALENDER AKADEMIK</span>
	    			<table id="table_report" class="responsive-table highlight striped">
		            <thead>
		              <tr>
		                <th>KEGIATAN</th>
		                <th>TANGGAL MULAI</th>
		                <th>TANGGAL SELESAI</th>
		              </tr>
		            </thead>

		            <tbody>
		              <?php
		              if(empty($kalender))
		              {
		                echo '<tr><td colspan="4">Tidak ada data Kegiatan yang tersimpan dalam database.</td></tr>';
		              }else{
		                foreach($kalender as $n)
		                {
		                  echo '<tr>';
		                  echo '<td>';
		                  echo '<b>'.$n->event.'</b>';
		                  echo '</td>';
		                  echo '<td>'.tanggal3($n->tanggal_kalender).'</td>';
		                  echo '<td>'.tanggal3($n->tanggal_selesai).'</td>';
		                  echo "</div>";
		                  echo '</td>';
		                  echo '</tr>';
		                }
		              }
		              ?>
		            </tbody>
		          </table>
	    		</div>
    		</div>
    	</div>
    </div>
  </main>
</div>
